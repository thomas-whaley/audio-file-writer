# Audio writer

This program takes a buffer of bytes and turns it into an audio file (only wav supported at the moment)

## Example Program

```java
public class Main {
    public static void main(String[] args) throws IOException {
        byte[] bytes = Files.readAllBytes(Path.of("arbitrary file"));

        AudioFile audioFile = new Builder().fileName("test.wav")
                .bitsPerSample(16)
                .sampleRate(44100)
                .numChannels(2)
                .bytes(bytes)
                .build();
        audioFile.write(new WavWriter());
    }
}
```
