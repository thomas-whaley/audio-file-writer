import org.junit.Test;
import thomas.writer.WavWriter;

import java.util.Arrays;

import static org.junit.Assert.assertArrayEquals;

public class AudioSaverTest {
    @Test
    public void testWriteBytesBigEndian() {
        byte[] input = new byte[4];
        WavWriter.writeIntBytesBigEndian(input, 0, 4, 0x01020304);
        assertArrayEquals(new byte[] {
                (byte) 0x01,
                (byte) 0x02,
                (byte) 0x03,
                (byte) 0x04,
        }, input);
    }

    @Test
    public void testWriteBytesLittleEndian() {
        byte[] input = new byte[4];
        WavWriter.writeIntBytesLittleEndian(input, 0, 4, 0x01020304);
        assertArrayEquals(new byte[] {
                (byte) 0x04,
                (byte) 0x03,
                (byte) 0x02,
                (byte) 0x01,
        }, input);
    }
    @Test
    public void testWriteBytesLittleEndianOffset() {
        byte[] input = new byte[8];
        Arrays.fill(input, (byte)0);
        WavWriter.writeIntBytesLittleEndian(input, 4, 4, 0x01020304);
        assertArrayEquals(new byte[] {
                (byte) 0,
                (byte) 0,
                (byte) 0,
                (byte) 0,
                (byte) 0x04,
                (byte) 0x03,
                (byte) 0x02,
                (byte) 0x01,
        }, input);
    }
}
