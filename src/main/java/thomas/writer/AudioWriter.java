package thomas.writer;

import thomas.builder.AudioFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public interface AudioWriter {
    byte[] audioFileBytes(AudioFile audioFile);

    default void write(AudioFile audioFile) {
        File file = new File(audioFile.getFileName());
        byte[] bytes = audioFileBytes(audioFile);
        try (FileOutputStream outputStream = new FileOutputStream(file)) {
            outputStream.write(bytes);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
