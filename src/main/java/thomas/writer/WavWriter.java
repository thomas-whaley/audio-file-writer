package thomas.writer;

import thomas.builder.AudioFile;
import thomas.writer.AudioWriter;

public class WavWriter implements AudioWriter {
    public static void writeIntBytesBigEndian(byte[] bytes, int start, int numBytes, int data) {
        // 0, 1, 2, 3, 4, 5
        // 1, 0, 3, 2, 5, 4
        for (int i = 0; i < numBytes; i ++) {
            int index = i;
            int shiftBy = (numBytes - index - 1) * 8;
            int mask = 0xff << shiftBy;
            int maskedData = (data & mask);
            bytes[start + i] = (byte) (maskedData >> shiftBy);
        }
    }

    public static void writeIntBytesLittleEndian(byte[] bytes, int start, int numBytes, int data) {
        // 0, 1, 2, 3, 4, 5
        // 1, 0, 3, 2, 5, 4
        for (int i = 0; i < numBytes; i ++) {
            int index = (numBytes - i - 1);
            int shiftBy = (numBytes - index - 1) * 8;
            int mask = 0xff << shiftBy;
            int maskedData = (data & mask);
            bytes[start + i] = (byte) (maskedData >> shiftBy);
        }
    }

    public byte[] wavFile(int chunkSize, int subchunk1Size, int audioFormat, int numChannels, int sampleRate, int byteRate, int blockAlign, int bitsPerSample, int subchunk2Size, byte[] data) {
        int totalSize = data.length + 44;
        byte[] bytes = new byte[totalSize];
        // RIFF Header ---
        // ChunkID = RIFF
        writeIntBytesBigEndian(bytes, 0, 4, 0x52494646);
        // ChunkSize
        writeIntBytesLittleEndian(bytes, 4, 4, chunkSize);
        // Format = WAVE
        writeIntBytesBigEndian(bytes, 8, 4, 0x57415645);
        // WAVE Header ---
        // Subchunk1ID = "fmt "
        writeIntBytesBigEndian(bytes, 12, 4, 0x666d7420);
        // Subchunk1Size
        writeIntBytesLittleEndian(bytes, 16, 4, subchunk1Size);
        // AudioFormat
        writeIntBytesLittleEndian(bytes, 20, 2, audioFormat);
        // NumChannels
        writeIntBytesLittleEndian(bytes, 22, 2, numChannels);
        // SampleRate
        writeIntBytesLittleEndian(bytes, 24, 4, sampleRate);
        // ByteRate
        writeIntBytesLittleEndian(bytes, 28, 4, byteRate);
        // BlockAlign
        writeIntBytesLittleEndian(bytes, 32, 4, blockAlign);
        // BitsPerSample
        writeIntBytesLittleEndian(bytes, 34, 2,bitsPerSample);

        // Data chunk
        // Subchunk2ID = "data"
        writeIntBytesBigEndian(bytes, 36, 4, 0x64617461);
        // Subchunk2Size
        writeIntBytesLittleEndian(bytes, 40, 4, subchunk2Size);

        System.arraycopy(data, 0, bytes, 44, data.length);
        return bytes;
    }

    @Override
    public byte[] audioFileBytes(AudioFile audioFile) {
        int dataSize = audioFile.getBytes().length;

        int subchunk1Size = 16; // PCM
        int subchunk2Size = dataSize;

        int chunkSize = 4 + (8 + subchunk1Size) + (8 + subchunk2Size);

        int byteRate = audioFile.getSampleRate() * audioFile.getNumChannels() * audioFile.getBitsPerSample() / 8;
        int blockAlign = audioFile.getNumChannels() * audioFile.getBitsPerSample() / 8;

        return wavFile(chunkSize,
                subchunk1Size,
                1,
                audioFile.getNumChannels(),
                audioFile.getSampleRate(),
                byteRate,
                blockAlign,
                audioFile.getBitsPerSample(),
                subchunk2Size, audioFile.getBytes());
    }
}
