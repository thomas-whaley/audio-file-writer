package thomas.builder;

import java.util.Optional;

public class Builder {
    private Optional<String> fileName = Optional.empty();
    private Optional<Integer> numChannels = Optional.empty();
    private Optional<Integer> sampleRate = Optional.empty();
    private Optional<Integer> bitsPerSample = Optional.empty();
    private Optional<byte[]> bytes = Optional.empty();

    public Builder fileName(String fileName) {
        this.fileName = Optional.of(fileName);
        return this;
    }

    public Builder numChannels(int numChannels) {
        if (numChannels != 1 && numChannels != 2) {
            throw new IllegalArgumentException("Expects number of channels to be either 1 (mono) or 2 (stereo). Got " + numChannels);
        }
        this.numChannels = Optional.of(numChannels);
        return this;
    }

    public Builder sampleRate(int sampleRate) {
        this.sampleRate = Optional.of(sampleRate);
        return this;
    }

    public Builder bitsPerSample(int bitsPerSample) {
        if (bitsPerSample != 8 && bitsPerSample != 16) {
            throw new IllegalArgumentException("Expects bits per sample to be either 8 or 16. Got " + bitsPerSample);
        }
        this.bitsPerSample = Optional.of(bitsPerSample);
        return this;
    }

    public Builder bytes(byte[] bytes) {
        this.bytes = Optional.of(bytes);
        return this;
    }

    public AudioFile build() {
        return new AudioFile(this.fileName.get(), this.numChannels.get(), this.sampleRate.get(), this.bitsPerSample.get(), this.bytes.get());
    }
}
