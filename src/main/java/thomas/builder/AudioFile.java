package thomas.builder;

import thomas.writer.AudioWriter;

public class AudioFile {
    private final String fileName;
    private final int numChannels;
    private final int sampleRate;
    private final int bitsPerSample;
    private final byte[] bytes;

    AudioFile(String fileName, int numChannels, int sampleRate, int bitsPerSample, byte[] bytes) {
        this.fileName = fileName;
        this.numChannels = numChannels;
        this.sampleRate = sampleRate;
        this.bitsPerSample = bitsPerSample;
        this.bytes = bytes;
    }

    public String getFileName() {
        return fileName;
    }

    public int getNumChannels() {
        return numChannels;
    }

    public int getSampleRate() {
        return sampleRate;
    }

    public int getBitsPerSample() {
        return bitsPerSample;
    }

    public byte[] getBytes() {
        return bytes;
    }

    public void write(AudioWriter writer) {
        writer.write(this);
    }

    public int numSamples() {
        // DataLength = NumSamples * NumChannels * BitsPerSample/8
        return bytes.length / (numChannels * bitsPerSample) * 8;
    }
}
